<?php

use Illuminate\Http\Request;
use App\Notifications\LogabsenNotification;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\WithMappedCells;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/', function(){
   echo 'Halo';
});

 Route::get('sendnotif', function (Request $request) {
     $json = json_encode(['halo' => 'tes']);
  dd($json);
  $data = [
      'kode_absen' => '123',
      'hari' => 'senin',
      'mapel_id' => 'ipa',
      'rombel_id' => '5a',
      'guru_id' => '1987983349041001',
      'jml_siswa' => 30,
      'hadir' => 29,
      'ijin' => 1,
      'sakit' => 0,
      'alpa' => 0,
      'telat' => 0,
      'ket' => 'diabsen',
      'status' => 'aktif',
      'jurnal' => 'Penguapan materi padat'
  ];
  $json = json_encode($data);
  $logabsen = json_decode($json);
  $users = 'App\User'::where('level', 'guru')->get();
  $send = Notification::send($users, new LogabsenNotification($logabsen));
  //~ dd($send);
 });

Route::post('login', 'UserController@login');

Route::group(['middleware' => 'auth:api'], function() {
    Route::post('send-notif', function() {
        $data = [
          'sender' => Auth::user()->nip,
          'msg' => 'Jangan lama-lama'
      ];
      $json = json_encode($data);
      $logabsen = json_decode($json);
      $users = 'App\User'::where('level', 'guru')->get();
      $send = Notification::send($users, new LogabsenNotification($logabsen));
      return response()->json($send);
    });

    // Rombel
    Route::get('rombels', 'RombelController@index')->name('get-rombels');
    Route::post('import/rombels', 'RombelController@import')->name('import-rombels');
    Route::delete('rombel/{id}', 'RombelController@destroy')->name('hapus-rombel');
    Route::post('rombel', 'RombelController@create')->name('create-rombel');
    Route::get('rombels/export', 'RombelController@export')->name('export-rombels');

    //~ Sekolah
    Route::get('sekolahs', 'SekolahController@index')->name('sekolahsindex');
    Route::get('sekolah', 'SekolahController@show')->name('showsekolah');
    Route::post('sekolah', 'SekolahController@create')->name('createsekolah');
    Route::post('import/sekolahs', 'SekolahController@import')->name('importsekolah');
    Route::put('sekolah/{id}', 'SekolahController@update')->name('updatesekolah');
    Route::delete('sekolah/{id}', 'SekolahController@delete')->name('deletesekolah');

    Route::get('user', 'UserController@getUser');
    Route::get('users', 'UserController@index');
    Route::post('user', 'UserController@create')->name('create-user');
    Route::get('users/select', 'UserController@selectWali')->name('select-wali-kelas');
    Route::put('user/{id}', 'UserController@update')->name('update-user');
    Route::post('import/users', 'UserController@import')->name('import-users');
    Route::delete('user/{id}', 'UserController@delete')->name('delete-user');

    Route::post('siswa', 'SiswaController@create')->name('create-siswa');
    Route::put('siswa', 'SiswaController@update')->name('update-siswa');
    Route::delete('siswa/{id}', 'SiswaController@destroy')->name('delete-siswa');
    Route::post('create-siswa/{npsn}', 'SiswaController@create');
    Route::post('upload/foto-siswa/{nis}/{npsn}', 'SiswaController@uploadFoto');
    Route::get('siswas', 'SiswaController@index')->name('indexsiswa');
    Route::get('siswa/cari', 'SiswaController@cari');
    Route::get('siswa/export', 'SiswaController@export')->name('export-siswa');
    Route::post('siswa/import', 'SiswaController@import')->name('import-siswa');
    Route::post('siswa/foto/{nis}', 'SiswaController@upFoto');
    Route::get('nonmember', 'SiswaController@getNonMember')->name('get-nonmember');
    Route::get('member/{rombel_id}', 'SiswaController@getMember')->name('get-member');
    Route::put('siswas/keluarkan', 'SiswaController@keluarkan')->name('keluarkan-member');
    Route::put('siswas/masukkan', 'SiswaController@masukkan')->name('masukkan-member');


    // Mapels
    Route::get('mapels', 'MapelController@index')->name('index-mapel');
    Route::post('import/mapel', 'MapelController@import')->name('import-mapel');
    Route::post('mapel', 'MapelController@create')->name('create-mapel');
    Route::put('mapel/{id}', 'MapelController@update')->name('update-mapel');
    Route::delete('mapel/{id}', 'MapelController@destroy')->name('update-mapel');
    Route::get('mapels/export', 'MapelController@export')->name('export-mapel');

    // jadwals
    Route::get('jadwals', 'JadwalController@index')->name('index-jadwal');
    Route::post('jadwal', 'JadwalController@create')->name('create-jadwal');
    Route::delete('jadwal/{id}', 'JadwalController@destroy')->name('delete-jadwal');
    Route::post('import/jadwal', 'JadwalController@import')->name('import-jadwal');
    Route::put('jadwal/{id}', 'JadwalController@update')->name('update-jadwal');

    // Logabsen
    Route::get('logabsens', 'LogabsenController@index')->name('index-logabsen');
    Route::get('my-jadwals', 'LogabsenController@myJadwals')->name('myjadwals');
    Route::post('logabsens/create', 'LogabsenController@create')->name('create-logabsen');
    Route::get('logabsens/activate', 'LogabsenController@activate')->name('activate-logabsen');
    Route::put('logabsens/deactivate', 'LogabsenController@deactivate')->name('deactivate-logabsen');

    Route::get('absen/siswa/{kode_rombel}', 'LogabsenController@getSiswaAbsen')->name('get-siswa-absen');

    Route::post('sekolah', 'SekolahController@create');
    Route::get('my-sekolah', 'SekolahController@show');
    Route::post('import/sekolah', 'SekolahController@import');
    Route::post('import/ortu', 'OrtuController@import');
    Route::get('rombels', 'RombelController@index');

    Route::get('notifs', 'UserController@myNotifs');
});
