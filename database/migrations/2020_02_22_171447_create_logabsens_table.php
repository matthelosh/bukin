<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogabsensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logabsens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_absen', 191);
            $table->string('sekolah_id', 191);
            $table->string('hari', 10);
            $table->date('tanggal', 12);
            $table->string('guru_id', 30);
            $table->string('mapel_id', 30);
            $table->string('rombel_id', 30);
            $table->string('jamke_id', 30);
            $table->integer('jml_siswa')->length(3);
            $table->integer('hadir')->length(3);
            $table->integer('ijin')->length(3);
            $table->integer('sakit')->length(3);
            $table->integer('alpa')->length(3);
            $table->integer('telat')->length(3);
            $table->text('jurnal');
            $table->string('isActive', 1);
            $table->string('ket', 60);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logabsen');
    }
}
