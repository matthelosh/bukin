<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrtusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ortus', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sekolah_id', 30);
            $table->string('siswa_id', 30);
            $table->string('nama_ayah', 60);
            $table->string('pend_ayah', 60);
            $table->string('job_ayah', 60);
            $table->string('nama_ibu', 60);
            $table->string('pend_ibu', 60);
            $table->string('job_ibu', 60);
            $table->string('nama_wali', 60)->nullable();
            $table->string('pend_wali', 60)->nullable();
            $table->string('job_wali', 60)->nullable();
            $table->string('hp_ortu', 16)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ortus');
    }
}
