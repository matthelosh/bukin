<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIjinGurusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ijin_gurus', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sekolah_id', 30);
            $table->string('absen_id', 30);
            $table->string('guru_id', 30);
            $table->date('tanggal', 10);
            $table->string('keperluan', 191);
            $table->string('tugas_siswa', 191);
            $table->string('ket', 191);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_ijin_gurus');
    }
}
