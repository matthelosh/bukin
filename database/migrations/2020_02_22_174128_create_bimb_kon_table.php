<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBimbKonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bimb_kons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_konsul', 30);
            $table->string('sekolah_id', 30);
            $table->date('tanggal', 12);
            $table->string('guru_id', 30);
            $table->string('siswa_id', 30);
            $table->string('subject', 30);
            $table->text('uraian');
            $table->string('tindakan', 60);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bimb_kon');
    }
}
