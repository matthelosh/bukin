<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJurnalStafsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jurnal_stafs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sekolah_id', 30);
            $table->string('staf_id', 30);
            $table->date('tanggal', 12);
            $table->string('kode_jurnal', 12);
            $table->string('lokasi', 30);
            $table->string('kegiatan', 191);
            $table->text('uraian');
            $table->string('mulai', 20);
            $table->string('selesai', 20);
            $table->string('status', 100);
            $table->string('ket', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('juran_stafs');
    }
}
