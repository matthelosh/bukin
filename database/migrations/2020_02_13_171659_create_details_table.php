<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sekolah_id', 60);
            $table->string('siswa_id', 60);
            $table->string('bb', 10);
            $table->string('tb', 10);
            $table->string('rw_sakit', 100);
            $table->string('hobby', 100);
            $table->string('prestasi', 191);
            $table->string('bk_id', 30);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('details');
    }
}
