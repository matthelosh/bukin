<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsAlamatSekolahs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sekolahs', function (Blueprint $table) {
          $table->string('alamat', 60);
          $table->string('desa', 60);
          $table->string('kec', 60);
          $table->string('kab', 60);
          $table->string('prov', 60);
          $table->string('kode_pos', 6);
          $table->string('telp', 20)->nullable();
          $table->string('fax', 20)->nullable();
          $table->string('email', 60)->nullable();
          $table->string('website', 60)->nullable();
          $table->string('kepsek_id', 30);
          $table->string('bujur', 60);
          $table->string('lintang', 60);
          $table->string('gps', 5)->default('off');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sekolahs', function (Blueprint $table) {
            //
        });
    }
}
