<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sekolah_id', 30);
            $table->string('rombel_id', 30);
            $table->string('nis', 30);
            $table->string('nisn', 30);
            $table->string('nama_siswa', 80);
            $table->string('jk', 10);
            $table->string('agama', 10);
            $table->string('tempat_lahir', 70);
            $table->date('tanggal_lahir');
            $table->string('alamat', 60);
            $table->string('desa', 60);
            $table->string('kec', 60);
            $table->string('kab', 60);
            $table->string('prov', 60);
            $table->string('kode_pos', 6);
            $table->string('hp', 16);
            $table->string('email', 60)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siswas');
    }
}
