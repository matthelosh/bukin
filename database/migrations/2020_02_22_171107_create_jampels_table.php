<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJampelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jampels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sekolah_id', 30);
            $table->string('label', 30);
            $table->string('mulai', 30);
            $table->string('selesai', 30);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jampels');
    }
}
