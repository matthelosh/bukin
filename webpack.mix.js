const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin');

mix
	.webpackConfig({
		resolve: {
   			extensions: ['.js', '.json', '.vue', '.css', '.scss', '.sass'],
   			alias: {
   				'~':path.join(__dirname, './resources/js'),
   				'$comp': path.join(__dirname, './resources/js/components'),
   				'@views': path.join(__dirname, './resources/js/views'),
					'@lib': path.join(__dirname, './resources/js/lib')
   			}
   		},
	})
	.js('resources/js/app.js', 'public/js')
	.sass('resources/sass/app.scss', 'public/css');
