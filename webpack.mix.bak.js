const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin');

mix.options({
      hmrOptions: {
        host: 'localhost',
        port: 8080 
      },
		  extractVueStyles: true,
      host: 'localhost',
      port: 9000,
			processUrls: false,
			uglify: {
				parallel : 8,
				compress: false
			}
	})
	.js('resources/js/app.js', 'js')
   	.sass('resources/sass/app.scss', 'css')
   	.webpackConfig({
   		resolve: {
   			extensions: ['.js', '.json', '.vue', '.css', '.scss', '.sass'],
   			alias: {
   				'~':path.join(__dirname, './resources/js'),
   				'$comp': path.join(__dirname, './resources/js/components'),
   				'@views': path.join(__dirname, './resources/js/views')
   			}
   		},
   		module: {
   			rules: [
   				{
   					test: /\.js$/,
   					loader: 'babel-loader',
   					exclude: /node_modules\/(?!(vuetify)\/)/,
   				},
   				// {
   				// 	test: /\.(sc|c|a)ss$/,
   				// 	use: [
   				// 		'vue-style-loader',
   				// 		'css-loader',
   				// 		{
   				// 			loader: 'sass-loader',
   				// 			options: {
   				// 				implementation: require('sass'),
   				// 				sassOptions: {
   				// 					fiber: require('fibers'),
   				// 					indentedSyntax: true
   				// 				},
   				// 			},
   				// 		},
   				// 	]
   				// }
   			]
   		},
   		plugins:[
   			new VuetifyLoaderPlugin()
   		],
         // devServer: {
            // proxy: {
               // host: '0.0.0.0',
               // port: 9000
            // },
            // watchOptions: {
               // aggregateTimeout:200,
               // poll:5000
            // }
         // }
   	})
   .version()
   .sourceMaps();
