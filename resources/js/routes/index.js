import Vue from 'vue';

import VueRouter from 'vue-router';
import store from './../store'


import Hello from '@views/Hello'
import Login from '@views/Login'
import Siswa from '@views/Siswa'
import User from '@views/User'
import Rombel from '@views/Rombel'
import Home from  '@views/Home'
import Sekolah from  '@views/Sekolah'
import Mapel from '@views/Mapel'
import Jadwal from '@views/Jadwal'
import Logabsen from '@views/Logabsen'
import Report from '@views/Report'
import JurnalGuru from '@views/JurnalGuru'
import Settings from  '@views/Settings'
import Absen from '@views/Absen'
import NotFound from '@views/NotFound'

import mid from './../middleware'

Vue.use(VueRouter);

// function lazyLoad(view) {
//  return() => import(`./..//${view}.vue`)
// }

function guard(to, from, next) {
    var isAuthenticated = false;

    if (store.getters.isLoggedIn)
        isAuthenticated = true
    else
        isAuthenticated = false

    if(isAuthenticated)
    {
        next();
    }
    else
    {

        if (to.query.kunci == '7u2n4153k014h' && to.query.nip && to.query.token) {
            mid.autoLogin(to.query.nip, to.query.token)
                .then( res => {
                    next('/')
                })
                .catch(err=>{
                    console.log(err)
                })
        } else {
            next('/login')
        }

    }
}

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home,
        beforeEnter: guard,
        meta: {title: 'Home'}
    },
    {
        path: '/hello',
        name: 'hello',
        component: Hello,
        beforeEnter: guard,
        meta: {title: 'Hello'}
    },
    {
        path: '/login',
        name: 'login',
        component: Login,
        meta: {title: 'Login'}
    },
    {
        path: '/users',
        name: 'users',
        component: User,
        beforeEnter: guard,
        meta: {title: 'Pengguna'}
    },
    {
        path: '/sekolah',
        name: 'sekolah',
        component: Sekolah,
        beforeEnter: guard,
        meta: {title: 'Sekolah'}
    },
    {
        path: '/siswas',
        name: 'siswas',
        component: Siswa,
        beforeEnter: guard,
        meta: {title: 'Siswa'}
    },
    {
        path: '/rombels',
        name: 'rombels',
        component: Rombel,
        beforeEnter: guard,
        meta: {title: 'Rombel'}
    },
    {
      path: '/mapels',
      name: 'mapels',
      component: Mapel,
      beforeEnter: guard,
      meta: {title: 'Mapel'}
    },
    {
      path: '/jadwals',
      name: 'jadwals',
      component: Jadwal,
      beforeEnter: guard,
      meta: {title: 'Jadwal'}
    },
    {
      path: '/logabsens',
      name: 'logabsens',
      component: Logabsen,
      beforeEnter: guard,
      meta: {title: 'Log Absen'}
    },
    {
      path: '/reports',
      name: 'reports',
      component: Report,
      beforeEnter: guard,
      meta: {title: 'Laporan'}
    },
    {
      path: '/jurnalguru',
      name: 'jurnalguru',
      component: JurnalGuru,
      beforeEnter: guard,
      meta: {title: 'Jurnal Guru'}
    },
    {
        path: '/absen',
        name: 'absen',
        component: Absen,
        beforeEnter: guard,
        meta: {title: 'Presensi Siswa'}
    },
    {
        path: '/settings',
        name: 'settings',
        component: Settings,
        beforeEnter: guard,
        meta: {title: 'Pengaturan'}
    },

    {
        path: '/*',
        name: '404',
        component: NotFound,
        meta: { title: '404' }
    }
]

const router = new VueRouter({
    mode: 'history',
    routes
})



export default router
