let d = new Date()
let date = d.getDate()
let month = (d.getMonth())
let year = d.getFullYear()
let day = d.getDay()
let hour = d.getHours()
let minute = d.getMinutes()
let second = d.getSeconds()

const bulans = ['Januari', 'Pebruari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember']
const haris = ['Minggu','Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', ]


export default {
  hari : haris[day],
  bulan: bulans[month],
  tahun: year,
  jam: hour,
  menit: minute,
  detik: second,
  tanggalIni: date+' '+ bulans[month]+' '+year

}
