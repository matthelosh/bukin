import axios from 'axios'
import store from './../store'
let middleware = {
	autoLogin: ( nip , sekolah) => {
		return new Promise((resolve, reject) => {
			axios.post(store.getters.server+'/api/login', {nip:nip, kode_sekolah: sekolah})
					.then(res => {
						var data = res.data;
						localStorage.setItem('user-token', data.token)
						store.state.token = res.data.token
						resolve(data.status);
					})
					.catch(err => {
						reject(err);
					})
		})
	}
}

export default middleware