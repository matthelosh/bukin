import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        user: {},
        token: localStorage.getItem('user-token'),
        status: '',
        sekolah: {},
        overlay: false,
        server: 'http://192.168.0.101:9000',
        // server: 'http://localhost:9000',
        sekolahs : [],
        loader: false,
        menus : [],
        notifs : []
    },
    getters: {
        sekolahs : state => state.sekolahs,
        overlay: state => state.overlay,
        server: state => state.server,
        isLoggedIn: state => !!state.token,
        sekolah: state => state.sekolah,
        user: state => state.user,
        token: state => state.token,
        loader: state => state.loader,
        menus: state => state.menus,
        notifs: state => state.notifs

    },
    mutations: {
        SET_SEKOLAH: (state, sekolah) => {
            state.sekolah = sekolah
        },

        AUTH_REQUEST: (state) => {
            state.overlay = true
        },

        AUTH_DONE: (state) => {
            state.overlay = false,
            state.token = localStorage.getItem('user-token')
        },
        AUTH_LOGOUT: (state) => {
            state.user = {},
            state.token = localStorage.getItem('user-token')
        },
        SET_USER: (state, user) => {
            state.user = user
        },

        SET_SEKOLAHS: (state, sekolahs) => {
            state.sekolahs = sekolahs;
        },
        SET_MY_SEKOLAH: (state, sekolah) => {
            state.sekolah = sekolah
        },
        ON_LOADER: (state) => {
            state.loader = true
        },

        OFF_LOADER: (state) => {
            state.loader = false
        },

        SET_MENUS: (state, menus) => {
            state.menus = menus
        },
        SET_MY_NOTIFS: (state, notifs) => {
            state.notifs = notifs
        }

    },
    actions: {
        GET_MY_NOTIFS: (context) => {
            axios.get(context.getters.server + '/api/notifs', {'headers': {'Authorization': 'Bearer ' + context.getters.token}})
                     .then ( res => {
                        var notifs = res.data.notifs
                        console.log(notifs);
                        context.commit('SET_MY_NOTIFS', notifs);

                     })
                     .catch ( err => {
                        console.log(err)
                     })
        },
        ON_LOADER:(context) => {
            context.commit('ON_LOADER')
        },
        OFF_LOADER: (context) => {
            context.commit('OFF_LOADER')
        },
        AUTH_LOGIN: (context, user) => {
            // context.commit('AUTH_REQUEST')
            return new Promise((resolve, reject) => {


                axios.post(context.getters.server+'/api/login', user)
                    .then(res => {
                        var data = res.data;
                        localStorage.setItem('user-token', data.token)
                        context.commit('AUTH_DONE');
                        resolve(data.status);
                    })
                    .catch(err => {
                        context.commit('AUTH_DONE');
                        reject(err);
                    })
                })
        },
        SET_USER: (context) => {
            context.commit('AUTH_REQUEST')
            return new Promise((resolve, reject) => {
                axios.get(context.getters.server + '/api/user', {headers: {'Authorization': 'Bearer ' + context.getters.token }})
                    .then(res => {
                        var user = res.data.user;
                        localStorage.setItem('level', user.level)
                        context.commit('SET_USER', user);
                        if ( user.level == 'superadmin') {
                            context.dispacth('SET_SEKOLAHS')
                            //~ context.dispacth('GET_MENUS')
                        } else {
                            //~ context.dispatch('SET_MY_SEKOLAH', user.sekolah_id)
                            //~ context.dispatch('GET_MENUS')
                        }
                        resolve('ok');
                    })
                    .catch(err => {
                        reject(err);
                    })
            })
        },

        GET_MENUS: (context) => {
            axios.get(context.getters.server + '/api/menus', {headers: {'Authorization': 'Bearer ' + context.getters.token}})
                .then(res => {
                    context.commit('SET_MENUS', res.data.menus)
                })
                .catch(err=>{
                    console.log(err)
                })
        },

        SET_SEKOLAHS: (context) => {
            axios.get(context.getters.server + '/api/sekolahs', {headers: {'Authorization': 'Bearer ' + context.getters.token }})
                 .then(res => {
                    if (res.data.status == 'sukses') {
                        context.commit(SET_SEKOLAHS, res.data.sekolahs)
                    } else {
                        alert(res.data.msg);
                    }
                 })
                 .catch(err=>{
                    alert(err)
                 })
        },
        SET_MY_SEKOLAH: (context, npsn) => {
            axios.get(context.getters.server + '/api/my-sekolah/', {headers: {'Authorization': 'Bearer '+context.getters.token}})
                .then(res => {
                    context.commit('SET_SEKOLAH', res.data.sekolah)
                })
                .catch(err=>{
                    alert(err)
                })
        },

        AUTH_LOGOUT: (context) => {
            context.commit('AUTH_REQUEST')
            return new Promise((resolve, reject) => {
                localStorage.removeItem('user-token')
                context.commit('AUTH_LOGOUT')
                resolve('ok');
            })
        }
    }
})

export default store
