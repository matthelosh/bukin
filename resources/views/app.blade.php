<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Jurnal+') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    {{-- <link rel="stylesheet" href="{{ mix('css/app.css') }}"> --}}
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <!-- Styles -->
        <style>

        </style>
    </head>
    <body>
        <div id="app">
            <app>
            </app>
        </div>

       {{-- <script src="{{ mix('js/app.js') }}"></script> --}}
       <script src="{{ asset('js/app.js') }}"></script>
        {{-- <script>
            window.user  = @json([
                'user' => auth()->user(),
            ])
        </script> --}}
    </body>
</html>
