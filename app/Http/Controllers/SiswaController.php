<?php

namespace App\Http\Controllers;

use App\Siswa;
use App\Exports\SiswaExport;
use App\Imports\SiswaImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\MessageBag;


class SiswaController extends Controller
{

     /* Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $npsn = Auth::user()->sekolah_id;
      try {
        $siswas = ($npsn != '0') ? Siswa::where('sekolah_id', $npsn)->get() : Siswa::all();

        return response()->json(['status' => 'sukses', 'msg' => 'Data siswa', 'siswas' => $siswas]);
      } catch (\Exception $e) {
        return response()->json(['status' => 'gagal', 'msg' => $e->getCode() . ':' . $e->getMessage()]);
      }

    }

    public function import(Request $request)
    {
      $file = $request->file('file');
      $file_name = rand().$file->getClientOriginalName();
      try {
        $file->move('files', $file_name);
        Excel::import(new SiswaImport, public_path('/files/'.$file_name));
        return response()->json(['status' => 'sukses', 'msg' => 'Data Siswa diimpor']);
      } catch (\Exception $e) {
        return response()->json(['status' => 'gagal', 'msg' => $e->getCode() .':'.$e->getMessage()]);
      }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

      $npsn = Auth::user()->sekolah_id;
        $foto = $request->file('foto');

        $check = Siswa::where(['sekolah_id' => $npsn, 'nis' => $request->nis])->first();
        if ($check != null ) {
          return response()->json(['status' => 'gagal', 'msg' => 'NIS '.$request->nis.' telah digunakan siswa [Nama: '.$check->nama_siswa.'NIS: '.$check->nis.']']);
        }
        try {
          $siswa = Siswa::firstOrCreate([
            'sekolah_id' => $npsn,
            'rombel_id' => $request->rombel_id,
            'nis' => $request->nis,
            'nisn' => $request->nisn,
            'nama_siswa' => $request->nama_siswa,
            'jk' => $request->jk,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'alamat' => $request->alamat,
            'desa' => $request->desa,
            'kec' => $request->kec,
            'kab' => $request->kab,
            'prov' => $request->prov,
            'kode_pos' => $request->kode_pos,
            'hp' => $request->hp,
            'email' => $request->email

          ]);
          // dd($siswa);
            return response()->json(['status' => 'sukses', 'msg' => 'Data siswa: '. $request->nama_siswa . ' disimpan.']);
        } catch (\Exception $e) {
          // return response()->json(['status' => 'gagal', 'msg' => $e->getCode() . ':' . $e->getMessage()]);

          return response()->json(['status' => 'gagal', 'msg' => $e->getCode() . ':' . $e->getMessage()]);
        }
    }

    public function upFoto(Request $request, $nis)
    {
      $foto = $request->file('file');
      $npsn = Auth::user()->sekolah_id;
      $nama_file = $npsn.'_'.$nis.'.jpg';
      try {
        $foto->move('img/siswas',$nama_file);
        return response()->json(['status' => 'sukses', 'msg' => 'Foto siswa disimpan']);
      } catch(\Exception $e)
      {
        return response()->json(['status' => 'gagal', 'msg' => $e->getCode(). ':' .$e->getMessage()]);
      }
    }

    public function uploadFoto(Request $request, $nis, $npsn)
    {
        $foto = $request->file('file');
        $nama_file = $npsn.'_'.$nis.'.jpg';
        try {
            $foto->move('img/siswas',$nama_file);
            return response()->json(['status' => 'sukses', 'msg' => 'Foto Siswa disimpan.']);

        } catch (\Exception $e) {
            return response()->json(['status' => 'gagal', 'msg' => $e->getCode().':'.$e->getMessage()]);
        }
    }

    public function cari(Request $request)
    {
        $q = $request->query('q');
        $npsn = $request->user()->sekolah_id;
        try {
            $res = Siswa::where('nama_siswa', 'like', '%'.$q.'%')->where('sekolah_id', $npsn)->orWhere('nisn', 'like', '%'.$q.'%')->orWhere('nis', 'like', '%'.$q.'%')->with('rombels', 'ortus')->orderBy('rombel_id')->get();

            return response()->json(['status' => 'sukses', 'msg' => 'Siswa ditemukan.', 'data' => $res]);

        }
        catch(\Exception $e)
        {
            return response()->json(['status' => 'gagal', 'msg' => $e->getCode().':'.$e->getMessage()]);
        }

    }

    public function getMember(Request $request, $rombel_id)
    {
      $npsn = Auth::user()->sekolah_id;
      try {
        $members = Siswa::where(['sekolah_id' => $npsn, 'rombel_id' => $rombel_id])->get();
        return response()->json(['status' => 'sukses', 'msg' => 'Data member', 'members' => $members]);
      } catch (\Exception $e) {
        return response()->json(['status' => 'gagal', 'msg' => $e->getCode().':'.$e->getMessage()]);
      }

    }
    public function getNonMember(Request $request)
    {
      $npsn = Auth::user()->sekolah_id;
      try {
        $nonmembers = Siswa::where(['sekolah_id' => $npsn, 'rombel_id' => '0' ])->get();
        return response()->json(['status' => 'sukses', 'msg' => 'Data Non Member', 'nonmembers' => $nonmembers]);
      } catch (\Exception $e) {
        return response()->json(['status' => 'sukses', 'msg' => $e->getCode().':'.$e->getMessage()]);
      }

    }

    public function Keluarkan(Request $request)
    {
      $datas = json_decode($request->input('siswas'));
      // dd($datas);
      try {
        foreach($datas as $data)
        {
            Siswa::findOrFail($data->id)->update(['rombel_id' => '0']);
        }
        return response()->json(['status' => 'sukses', 'msg' => 'Siswa dikeluarkan dari rombel']);
      } catch (\Exception $e) {
        return response()->json(['status'=>'gagal', 'msg' => $e->getCode().':'.$e->getMessage()]);
      }

    }

    public function masukkan(Request $request)
    {
      $datas = json_decode($request->input('siswas'));
      $rombel_id = $request->query('rombel_id');
      try {
        foreach($datas as $data)
        {
            Siswa::findOrFail($data->id)->update(['rombel_id' => $rombel_id]);
        }
        return response()->json(['status' => 'sukses', 'msg' => 'Siswa dikeluarkan dari rombel']);
      } catch (\Exception $e) {
        return response()->json(['status'=>'gagal', 'msg' => $e->getCode().':'.$e->getMessage()]);
      }
    }

    public function export()
    {
      try {
        return Excel::download(new SiswaExport, 'data_siswa.xlsx');
      }
      catch(\Exception $e)
      {
        return response()->json(['status' => 'gagal', 'msg' => $e->getCode() .':'. $e->getMessage()]);
      }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function show(Siswa $siswa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function edit(Siswa $siswa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      try {
        $siswa = Siswa::find($request->query('id'));
        $file = $request->file('file');
       // $data = json_decode($request->all());
        dd($request->all());
        return response()->json(['status' => 'sukses', 'msg' => 'dat siswa diperbarui']);
      }
      catch (\Exception $e)
      {
        return response()->json(['status' => 'gagal', 'msg' => $e->getCode().':'.$e->getMessage()]);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
      $npsn = Auth::user()->sekolah_id;
      try {
        Siswa::where(['sekolah_id' => $npsn, 'id' => $id])->delete();
        return response()->json(['status' => 'sukses', 'msg' => 'Siswa [NIS:'.$nis .'] dihapus']);
      } catch (\Exception $e)
      {
        return response()->json(['status' => 'gagal', 'msg' => $e->getCode() . ':' .$e->getMessage()]);
      }
    }
}
