<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Imports\UserImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    private $successStatus = 200;

    public function login(Request $request)
    {
        if ($request->input('nip')) {
            $sekolah = 'App\Sekolah'::where('kode_sekolah', $request->input('kode_sekolah'))->first();
            if(Auth::attempt(['nip' => request('nip'), 'password' => '12345', 'sekolah_id' => $sekolah->npsn])) {
                $user = Auth::user();

                $token = $user->createToken('token')->accessToken;


                return response()->json(['status' => 'sukses', 'msg' => 'Login Berhasil', 'token' => $token]);
            } else {
                return response()->json(['status' =>'Gagal', 'msg' => 'Login Gagal', 'code' => 401], 401);
            }
        } else {
            if(Auth::attempt(['email' => request('email'), 'password' => request('password')])) {

                $user = Auth::user();

                $token = $user->createToken('token')->accessToken;


                return response()->json(['status' => 'sukses', 'msg' => 'Login Berhasil', 'token' => $token]);
            } else {
                return response()->json(['status' =>'Gagal', 'msg' => 'Login Gagal', 'code' => 401], 401);
            }
        }

    }

    public function getUser(Request $request)
    {
        $user = 'App\User'::where('id', Auth::user()->id)->first();
        return response()->json(['status' => 'sukses', 'msg' => 'Data User Login', 'user' => $user]);
    }

    public function index()
    {
      try {
          if(Auth::user()->level == 'superadmin' && Auth::user()->sekolah_id == '0') {
            $users = User::orderBy('id')->get();
          } else if(Auth::user()->level == 'admin'){
            $npsn = Auth::user()->sekolah_id;
            $users = User::where('sekolah_id', $npsn)->where('level' , '!=', 'admin')->get();
          }
            return response()->json(['status' => 'sukses', 'msg' => 'Data Semua Pengguna', 'users' => $users]);
          } catch (\Exception $e) {
            return response()->json(['gagal' => 'sukses', 'msg' => $e->getCode() . ':' . $e->getMessage()]);
        }
    }

    public function selectWali(Request $request)
    {
      $npsn = Auth::user()->sekolah_id;
      try {
        $gurus = User::where(['sekolah_id' => $npsn, 'level' => 'guru'])->get();
        return response()->json(['status' => 'sukses', 'msg' => 'Data Guru', 'gurus' => $gurus]);
      } catch (\Exception $e) {
        return response()->json(['status' => 'gagal', 'msg' => $e->getCode() . ':' . $e->getMessage()]);
      }

    }
    public function myNotifs(Request $request)
    {
        $user = Auth::user();
        $user1 = User::find(2);


        $notifs = $user->notifications;
        //~ dd($notifs);
        return response()->json([ 'status' => 'sukses', 'msg' => 'User Notif', 'notifs' => $notifs ]);
    }

    public function import(Request $request)
    {
        $file = $request->file('file');
        $nama_file = rand().$file->getClientOriginalName();
        $file->move('files', $nama_file);
        try {
            Excel::import(new UserImport, public_path('/files/'.$nama_file));

            return response()->json(['status' => 'sukses', 'msg' => 'data User diimport']);
        } catch (\Exception $e)
        {
            return response()->json(['status' => 'gagal', 'msg' => $e->getCode().':'.$e->getMessage()], 500);
        }
    }

    public function create(Request $request)
    {
        try {
            User::create([
                'nip' => $request->input('nip'),
                'username' => $request->input('username'),
                'email' => $request->input('email'),
                'password' => Hash::make($request->input('password')),
                'fullname' => $request->input('fullname'),
                'level' => $request->input('level'),
                'role' => $request->input('role'),
                'sekolah_id' => $request->input('sekolah_id'),
                'hp' => $request->input('hp'),
            ]);

            return response()->json(['status' => 'sukses', 'msg' => 'User dibuat']);
        }
        catch (\Exception $e)
        {
            return response()->json(['status' => 'gagal', 'msg' => $e->getCode().':'.$e->getMessage()]);
        }
    }

    public function update(Request $request, $id)
    {
        $data = $request->password ? $request->all() : $request->except('password');
        try {
            $user = User::findOrFail($id);
            $nama_lama = $user->fullname;
            $user->update($data);

            return response()->json(['status' => 'suksesk', 'msg' => 'Data '. $nama_lama . ' diperbarui.']);

        } catch(\Exception $e) {
            return response()->json(['status' => 'gaga', 'msg' => $e->getCode(). ': ' .$e->getMessage()]);
        }
    }

    public function delete(Request $request, $id)
    {
        $user = User::findOrFail($id);
        try
        {
            $user->delete();
            return response()->json(['status' => 'sukses', 'msg' => 'Data '.$user->fullname . ' dihapus']);
        }
        catch (\Exception $e)
        {
            return response()->json(['status' => 'gagal', 'msg' => $e->getCode().':'.$e->getMessage()], 500);
        }
    }

}
