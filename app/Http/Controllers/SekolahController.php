<?php

namespace App\Http\Controllers;

use App\Sekolah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Imports\SekolahImport;
use Maatwebsite\Excel\Facades\Excel;

class SekolahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $datas = Sekolah::with('users')->get();

            return response()->json(['status' => 'sukses', 'msg' => 'Data Semua Sekolah', 'sekolahs' => $datas ], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => 'gagal', 'msg' => $e->getCode().':'.$e->getMessage()]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try {
            Sekolah::create([
                'npsn' => $request->input('npsn'),
                'kode_sekolah' => $request->input('kode_sekolah'),
                'nama_sekolah' => $request->input('nama_sekolah')
            ]);

            return response()->json(['status' => 'sukses', 'msg' => 'Data sekolah baru disimpan']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'gagal', 'msg' => $e->getCode().':'.$e->getMessage()]);
        }
    }

    public function import(Request $request)
    {
        // $this->validate($request, [
        //     'file' => 'required|mimes:csv,xls,xlsx'
        // ]);

        $file = $request->file('file');
        $nama_file = rand().$file->getClientOriginalName();
        $file->move('files', $nama_file);
        try {
            Excel::import(new SekolahImport, public_path('/files/'.$nama_file));

            return response()->json(['status' => 'sukses', 'msg' => 'data Sekolah diimpor']);
        }
        catch (\Exception $e)
        {
            return response()->json(['status' => 'gagal', 'msg' => $e->getCode().':'.$e->getMessage()]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sekolah  $sekolah
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
      $npsn = Auth::user()->sekolah_id;
      $kepsek = 'App\User'::where(['sekolah_id' => $npsn, 'level' => 'ks'])->first();
      $sekolah = Sekolah::where('npsn', $npsn)->first();
      $sekolah['kepsek'] = $kepsek;

        return response()->json(['status' => 'sukses', 'msg' => 'Sekolah Anda', 'sekolah' => $sekolah]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sekolah  $sekolah
     * @return \Illuminate\Http\Response
     */
    public function edit(Sekolah $sekolah)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sekolah  $sekolah
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      try {
        $sekolah = Sekolah::findOrFail($id)->update($request->all());
        return response()->json(['status' => 'sukses', 'msg' => 'Data '.$request->nama_sekolah.' diperbarui.']);
      } catch (\Exception $e) {
        return response()->json(['status' => 'gagal', 'msg' => $e->getCode().':'.$e->getMessage()], 422);
      }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sekolah  $sekolah
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sekolah $sekolah)
    {
        //
    }
}
