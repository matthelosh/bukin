<?php

namespace App\Http\Controllers;

use App\Rombel;
use App\Imports\RombelImport;
use Illuminate\Http\Request;
use App\Exports\RombelsExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;

class RombelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $npsn = Auth::user()->sekolah_id;
        if($npsn != '0') {
        // dd($request->user());
            $rombels = Rombel::where('sekolah_id', $npsn)->with('siswas', 'gurus')->get();
        } else {
            $rombels = Rombel::with('siswas', 'sekolahs', 'gurus')->get();
        }
        return response()->json(['status' => 'sukses', 'msg' => 'Data Rombel', 'rombels' => $rombels]);
    }

    public function import(Request $request)
    {
      $file = $request->file('file');
      try {
        $file_name = rand() . $file->getClientOriginalName();
        $file->move('files', $file_name);
        Excel::import(new RombelImport, public_path('/files/'.$file_name));

        return response()->json(['status' => 'sukses', 'msg' => 'Data Rombel Diimpor.']);
      } catch (\Exception $e) {
        return response()->json(['status' => 'gagal', 'msg' => $e->getCode() . ':' . $e->getMessage()]);
      }

    }

    public function export()
    {
      return Excel::download(new RombelsExport, 'data-rombel.xlsx'); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      try {
        Rombel::create([
          'sekolah_id' => Auth::user()->sekolah_id,
          'kode_rombel' => $request->kode_rombel,
          'nama_rombel' => $request->nama_rombel,
          'status' => $request->status,
          'tingkat' => $request->tingkat,
          'guru_id' => $request->guru_id,
        ]);
        return response()->json(['status' => 'sukses', 'msg' => 'Data Rombel dibuat']);
      } catch (\Exception $e) {
        return response()->json(['status' => 'gagal', 'msg' => $e->getCode() . ':' . $e->getMessage()]);
      }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rombel  $rombel
     * @return \Illuminate\Http\Response
     */
    public function show(Rombel $rombel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rombel  $rombel
     * @return \Illuminate\Http\Response
     */
    public function edit(Rombel $rombel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rombel  $rombel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rombel $rombel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rombel  $rombel
     * @return \Illuminate\Http\Response
     */
    public function destroy($rombel)
    {
      try {
        Rombel::find($rombel)->delete();
        return response()->json(['status' => 'sukses', 'msg' => 'Rombel dihapus']);
      } catch (\Exception $e) {
        return response()->json(['status' => 'gagal', 'msg' => $e->getCode() . ':' . $e->getMessage()]);
      }

    }
}
