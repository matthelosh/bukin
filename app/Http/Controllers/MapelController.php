<?php

namespace App\Http\Controllers;

use App\Mapel;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\MapelExport;
use App\Imports\MapelImport;

class MapelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $npsn = Auth::user()->sekolah_id;
      $mapels;
      try {
        if ($npsn == '0') {
          $mapels = Mapel::all();
        } else {
          $mapels = Mapel::where('sekolah_id', $npsn)->get();
        }

        return response()->json(['status' => 'sukses', 'msg' => 'data Mapel', 'mapels' => $mapels]);
      } catch(\Exception $e)
      {
        return response()->json(['status' => 'gagal', 'msg' => $e->getCode().':'.$e->etMessage()]);
      }

    }

    public function export()
    {
      return Excel::download(new MapelExport, 'data-mapel.xlsx'); 
    }

    public function import(Request $request)
    {
      $file = $request->file('file');
      $file_name = rand().$file->getClientOriginalName();
      $file->move('files', $file_name);
      try {
        Excel::import(new MapelImport, public_path('/files/'.$file_name));
        return response()->json(['status' => 'sukses', 'msg' => 'Data Mapel diimpor']);
      } catch (\Exception $e) {
        return response()->json(['status' => 'sukses', 'msg' => $e->getCode().':'.$e->getMessage()]);
      }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      try {
        Mapel::create([
          'sekolah_id' => $request->sekolah_id,
          'kode_mapel' => $request->kode_mapel,
          'nama_mapel' => $request->nama_mapel
        ]);

        return response()->json(['status' => 'sukses', 'msg' => 'Data Mapel baru disimpan.']);
      } catch (\Exception $e) {
        return response()->json(['status' => 'gagal', 'msg' => $e->getCode().':'.$e->getMessage()], 422);
      }

    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mapel  $mapel
     * @return \Illuminate\Http\Response
     */
    public function show(Mapel $mapel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mapel  $mapel
     * @return \Illuminate\Http\Response
     */
    public function edit(Mapel $mapel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mapel  $mapel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      try {
        Mapel::findOrFail($id)->update([
          'sekolah_id' => $request->sekolah_id,
          'kode_mapel' => $request->kode_mapel,
          'nama_mapel' => $request->nama_mapel
        ]);
        return response()->json(['status' => 'sukses', 'msg' => 'Data Mapel diperbarui.']);
      } catch (\Exception $e) {
        return response()->json(['status' => 'gagal', 'msg' => $e->getCode().':'.$e->getMessage()]);
      }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mapel  $mapel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
      try {
        Mapel::findOrFail($id)->delete();
        return response()->json(['status' => 'sukses', 'msg' => 'Data Mapel diperbarui.']);
      } catch (\Exception $e) {
        return response()->json(['status' => 'gagal', 'msg' => $e->getCode().':'.$e->getMessage()], 422);
      }

    }
}
