<?php

namespace App\Http\Controllers;

use App\Jadwal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\JadwalImport;

class JadwalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $jadwal;
      try {
        if ( Auth::user()->sekolah_id == '0') {
          $jadwals = Jadwal::all();
        } else {
          $jadwals = Jadwal::where([
                        'sekolah_id' => Auth::user()->sekolah_id,
                        'status' => 'aktif'
                      ])->with('gurus', 'mapels', 'rombels')->get();
        }
        return response()->json(['status' => 'sukses', 'msg' => 'Data Jadwal', 'jadwals' => $jadwals]);
      } catch(\Exception $e)
      {
        return response()->json(['status' => 'gagal', 'msg' => $e->getCode().':'.$e->getMessage()], 500);
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      $kode_jadwal = $request->hari.$request->guru_id.$request->mapel_id.$request->rombel_id.$request->jamke;
      try {
        Jadwal::create([
          'sekolah_id' => $request->sekolah_id,
          'kode_jadwal' => $kode_jadwal,
          'hari' => $request->hari,
          'guru_id' => $request->guru_id,
          'mapel_id' => $request->mapel_id,
          'rombel_id' => $request->rombel_id,
          'jamke' => $request->jamke,
          'ket' => $request->ket,
          'status' => $request->status
        ]);

        return response()->json(['status' => 'sukses', 'msg' => 'Jadwal dibuat']);
      } catch (\Exception $e) {
        return response()->json(['status' => 'gagal', 'msg' => $e->getCode().':'.$e->getMessage()]);
      }

    }

    public function import(Request $request)
    {
      $file = $request->file('file');
      $file_name = rand().$file->getClientOriginalName();
      try {
        $file->move('files', $file_name);
        Excel::import(new JadwalImport, public_path('/files/'.$file_name));

        return response()->json(['status' => 'sukses', 'msg' => 'Data Jadwal diimpo']);
      } catch (\Exception $e) {
        return response()->json(['status' => 'gagal', 'msg' => $e->getCode().':'.$e->getMessage()], 500);
      }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Jadwal  $jadwal
     * @return \Illuminate\Http\Response
     */
    public function show(Jadwal $jadwal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Jadwal  $jadwal
     * @return \Illuminate\Http\Response
     */
    public function edit(Jadwal $jadwal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Jadwal  $jadwal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      try {
        Jadwal::findOrFail($id)->update($request->all());
        return response()->json(['status' => 'sukses', 'msg' => 'Jadwa '.$request->kode_jadwal.' diperbarui.']);
      } catch (\Exception $e)
      {
        return response()->json(['status' => 'gagal', 'msg' => $e->getCode().':'.$e->getMessage()]);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Jadwal  $jadwal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
      try {
        Jadwal::findOrFail($id)->delete();
        return response()->json(['status' => 'sukses', 'msg' => 'Data Jadwal dihapus.']);
      } catch(\Exception $e)
      {
        return response()->json(['status' => 'gagal', 'msg' => $e->getCode().':'.$e->getMessage()], 422);
      }
    }
}
