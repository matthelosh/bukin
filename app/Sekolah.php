<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sekolah extends Model
{
    protected $fillable = ['npsn', 'kode_sekolah', 'nama_sekolah', 'alamat', 'desa', 'kec', 'kab', 'prov', 'kode_pos', 'telp', 'fax', 'email', 'website', 'bujur', 'lintang', 'gps', 'kepsek_id', 'fullday', 'jml_jam', 'jenjang'];


    public function users()
    {
    	return $this->hasMany('App\User', 'sekolah_id', 'npsn');
    }

    public function siswas()
    {
    	return $this->hasMany('App\Siswa', 'sekolah_id', 'npsn');
    }

    public function rombels()
    {
    	return $this->hasMany('App\Rombel', 'sekolah_id', 'npsn');
    }

    public function ortus()
    {
    	return $this->hasMany('App\Ortu', 'sekolah_id', 'npsn');
    }

    public function kepsek()
    {
      $ks = 'App\User'::where('sekolah_id', $this->npsn)->first();
      return $ks;
    }

    public function kepseks()
    {
      return $this->belongsTo('App\User', 'kepsek_id', 'nip');
    }
}
