<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    protected $fillable = ['siswa_id', 'bb', 'tb', 'rw_sakit', 'hobby', 'prestasi', 'bk_id'];

    public function siswas()
    {
    	return $this->belongsTo('App\Siswa', 'siswa_id', 'nisn');
    }
}
