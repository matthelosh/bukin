<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = ['label', 'icon', 'url', 'level', 'isActive'];

    public function users()
    {
    	return $this->belongsTo('App\User', 'level', 'level');
    }
}
