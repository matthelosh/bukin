<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mapel extends Model
{
  protected $fillable = ['sekolah_id','kode_mapel', 'nama_mapel'];

  public function jadwals()
  {
    return $this->hasMany('App\Jadwal', 'mapel_id', 'kode_mapel');
  }

  public function logabsens()
  {
    return $this->hasMany('App\Logabsen', 'mapel_id', 'kodemapel');
  }
  public function absens()
  {
    return $this->hasMany('App\Absen', 'mapel_id', 'kodemapel');
  }
}
