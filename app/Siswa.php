<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $fillable = ['sekolah_id', 'ortu_id', 'rombel_id', 'nis', 'nisn', 'nama_siswa', 'jk', 'tempat_lahir', 'tanggal_lahir', 'alamat', 'desa', 'kec', 'kab', 'prov', 'kode_pos', 'hp', 'email'];


    public function sekolahs()
    {
    	return $this->belongsTo('App\Sekolah', 'sekolah_id', 'npsn');
    }

    public function ortus()
    {
    	return $this->hasOne('App\Ortu', 'siswa_id', 'nis');
    }

    public function rombels()
    {
    	return $this->belongsTo('App\Rombel', 'rombel_id', 'kode_rombel');
    }

    public function details()
    {
    	return $this->hasOne('App\Detail', 'siswa_id', 'nisn');
    }
}

