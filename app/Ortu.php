<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ortu extends Model
{
    protected $fillable = ['sekolah_id', 'siswa_id', 'ayah', 'ibu', 'wali', 'pend_ayah', 'job_ayah', 'pengh_ayah', 'nik-ayah', 'pend_ibu', 'job_ibu', 'pengh_ibu', 'nik_ibu', 'pend_wali', 'job_wali', 'pengh-wali', 'nik-wali'];

    public function siswas()
    {
    	return $this->belongsTo('App\Siswa', 'siswa_id', 'nis');
    }

    public function sekolahs()
    {
    	return $this->belongsTo('App\Sekolah', 'sekolah_id', 'npsn');
    }
}

