<?php

namespace App\Imports;

use App\Sekolah;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class SekolahImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Sekolah([
            'npsn' => $row['npsn'],
            'kode_sekolah' => $row['kode_sekolah'],
            'nama_sekolah' => $row['nama_sekolah']
        ]);
    }
}
