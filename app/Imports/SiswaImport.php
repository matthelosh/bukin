<?php

namespace App\Imports;

use App\Siswa;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\Auth;

class SiswaImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    // sekolah_id', 'ortu_id', 'rombel_id', 'nis', 'nisn', 'nama_siswa', 'jk', 'tempat_lahir', 'tanggal_lahir', 'alamat', 'desa', 'kec', 'kab', 'prov', 'kode_pos', 'hp', 'email'
    public function model(array $row)
    {
        return new Siswa([
          'sekolah_id' => Auth::user()->sekolah_id,
          'ortu_id' => $row['ortu_id'],
          'rombel_id' => $row['rombel_id'],
          'nisn' => $row['nisn'],
          'nis' => $row['nis'],
          'nama_siswa' => $row['nama_siswa'],
          'jk' => $row['jk'],
          'tempat_lahir' => $row['tempat_lahir'],
          'tanggal_lahir' => $row['tanggal_lahir'],
          'alamat' => $row['alamat'],
          'desa' => $row['desa'],
          'kec' => $row['kec'],
          'kab' => $row['kab'],
          'prov' => $row['prov'],
          'kode_pos' => $row['kode_pos'],
          'hp' => $row['hp'],
          'email' => $row['email'],
        ]);
    }
}
