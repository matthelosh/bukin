<?php

namespace App\Imports;

use App\Ortu;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ImportOrtus implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Ortu([
            'sekolah_id' => $row['sekolah_id'],
            'siswa_id' => $row['siswa_id'],
            'ayah' => $row['ayah'],
            'ibu' => $row['ibu'],
            'wali' => $row['wali'],
            'pend_ayah' => $row['pend_ayah'],
            'job_ayah' => $row['job_ayah'],
            'pengh_ayah' => $row['pengh_ayah'],
            'nik_ayah' => $row['nik_ayah'],
            'pend_ibu' => $row['pend_ibu'],
            'job_ibu' => $row['job_ibu'],
            'pengh_ibu' => $row['pengh_ibu'],
            'nik_ibu' => $row['nik_ibu'],
            'pend_wali' => $row['pend_wali'],
            'job_wali' => $row['job_wali'],
            'pengh_wali' => $row['pengh_wali'],
            'nik_wali' => $row['nik_wali']
        ]);
    }
}
