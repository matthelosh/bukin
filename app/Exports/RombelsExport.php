<?php

namespace App\Exports;

use App\Rombel;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\StringValueBinder;

class RombelsExport extends \PhpOffice\PhpSpreadsheet\Cell\StringValueBinder implements FromCollection, WithHeadings, WithCustomValueBinder
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
      $npsn = Auth::user()->sekolah_id;
      return Rombel::where('sekolah_id', $npsn)->select('sekolah_id','kode_rombel', 'nama_rombel', 'tingkat', 'guru_id', 'status')->get();

        // return Rombel::all();
    }

    public function headings():array
    {
      return ['sekolah_id', 'kode_rombel', 'nama_rombel', 'tingkat', 'guru_id', 'status'];
    }
}
