<?php

namespace App\Exports;

use App\Mapel;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\StringValueBinder;

class MapelExport extends \PhpOffice\PhpSpreadsheet\Cell\StringValueBinder implements FromCollection, WithHeadings, WithCustomValueBinder
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
      $npsn = Auth::user()->sekolah_id;
      return Mapel::where(['sekolah_id' => $npsn])->select('sekolah_id', 'kode_mapel', 'nama_mapel')->get();
    }

    public function headings():array
    {
      return ['NPSN', 'Kode Mapel', 'Nama Mapel'];
    }
}
