<?php

namespace App\Exports;

use App\Siswa;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\Auth;


class SiswaExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
      $npsn = Auth::user()->sekolah_id; 
        return Siswa::where('sekolah_id', $npsn)->select('nis', 'nisn', 'nama_siswa','rombel_id', 'jk', 'tempat_lahir', 'tanggal_lahir','alamat', 'desa', 'kec', 'kab', 'prov', 'hp', 'email')->get();
    }

    public function headings(): array
    {
    
      return ['NIS', 'NISN', 'NAMA', 'ROMBEL', 'JK', 'TEMPAT_LAHIR', 'TANGGAL_LAHIR', 'ALAMAT', 'DESA', 'KEC', 'KAB', 'PROV', 'HP', 'EMAIL'];
    }
}
