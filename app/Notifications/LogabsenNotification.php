<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Logabsen;

class LogabsenNotification extends Notification
{
    use Queueable;
    public $logabsen;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($logabsen)
    {
        $this->logabsen = $logabsen;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

 
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            // 'absen_id' => $this->logabsen->kode_absen,
            'log' => $this->logabsen
        ];
    }
}
